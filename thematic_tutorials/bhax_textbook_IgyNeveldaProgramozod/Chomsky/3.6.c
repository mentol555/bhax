#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
void jelkezelo(int signum)
{
	printf("Caught signal %d\n",signum);
	exit(signum);
}

int f(int a,int b)
{
	return a+b;

}
int g(int a)
{

	return ++a;
}
int x(int a)
{

	return ++a;
}
int main()
{
	typedef void (*sighandler_t)(int);
	sighandler_t signal(int signum, sighandler_t handler);
	if(signal(SIGINT,SIG_IGN)!=SIG_IGN)
		signal(SIGINT,jelkezelo);

	int i;
	int a=5;
	int tomb[5];
	int *s=&a; int *d=&a;
	int n=5;
	for(i=0;i<5;++i)
		printf("i=%d ",i);
	printf("\n");

	for(i=0;i<5;i++)
		printf("i=%d ",i);
	printf("\n");

	for(i=0;i<5;tomb[i]=i++)
		printf(" %d, ",tomb[i]);
	printf("\n");

	for(i=0;i<n && (*d++ = *s++);++i){}
	printf("\n");

	printf("%d %d",f(a,++a), f(++a,a));
	printf("\n");

	printf("%d %d",g(a),a);
	printf("\n");
	printf("%d %d",x(&a), a);

	printf("\n");
	return 0;
}