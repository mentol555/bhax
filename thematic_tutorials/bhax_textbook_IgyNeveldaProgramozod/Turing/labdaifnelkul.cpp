#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <cmath>

int main()
{
	WINDOW *ablak;
	ablak=initscr();
	
	int x=0;int y=0;
	int xnov=0;int ynov=0;
	int maxx;int maxy;
	getmaxyx( ablak, maxy, maxx );
	maxx=maxx*2; maxy=maxy*2;
	for(;;)
	{
		x=(x-1)%maxx;//x koord meghatarozasa
		xnov=(xnov+1)%maxx;//novekedesi ertek meghatarozasa
		
		y=(y-1)%maxy;//y koord meghatarozasa
		ynov=(ynov+1)%maxy;//novekedesi ertek meghatarozasa
		
		
		mvprintw(abs((y+(maxy-ynov))/2),abs((x+(maxx-xnov))/2),"o");//abszolut ertek szamitas, a lenyeg hogy bent tartsa az ablakban a labdat, ne engedje elszabadulni a szamokat es maga az abszolut pedig a folytonos pozitivitasert felel.	
		mvprintw(y,x,"o");
		
		refresh();
		
		usleep(100000);
	}
	return 0;
}
