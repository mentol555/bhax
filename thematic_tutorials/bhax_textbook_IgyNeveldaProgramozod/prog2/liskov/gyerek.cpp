#include <iostream>

using namespace std;

class szulo {
	public:
		void uzenet()
		{
			std::cout<<"Szulo uzenete";
		}
};

class gyerek : public szulo {
	public:
		void gyerekuzenete()
		{
			std::cout<<"Gyerek uzenete";
		}
};

int main()
{
	szulo* sz1 = new szulo();
	szulo* sz2 = new gyerek();
	sz1->uzenet();
	sz2->gyerekuzenete();
	return 0;
}