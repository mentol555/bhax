public class ExorTitkosító {//java program class-ja amiben megvalositjuk a titkositast
    
    public ExorTitkosító(String kulcsSzöveg,//kulcsszoveg
            java.io.InputStream bejövőCsatorna,//input stream
            java.io.OutputStream kimenőCsatorna)//output stream
            throws java.io.IOException {//hibakezeles
        
        byte [] kulcs = kulcsSzöveg.getBytes();//kulcs tombbe valo beolvasas
        byte [] buffer = new byte[256];//buffer tomb, amibe beolvassuk majd a szoveget az input streamrol, es ebbe fog letrejonni a titkositott valtozata a szovegnek
        int kulcsIndex = 0;//kulcs indexe
        int olvasottBájtok = 0;//hany bajtot olvastunk be

        while((olvasottBájtok =
                bejövőCsatorna.read(buffer)) != -1) {//ez a kulso while egyszeruen olvas a bufferbe, amig csak van mit
            
            for(int i=0; i<olvasottBájtok; ++i) {//ennek a forciklusnak a keretein belul fog letrejonni a titkositas
                
                buffer[i] = (byte)(buffer[i] ^ kulcs[kulcsIndex]);//a magikus XOR muvelet ami osszefesuli a buffer tombben levo aktualis karaktert a kulccsal
                kulcsIndex = (kulcsIndex+1) % kulcs.length;//kulcsindex valtoztatasa
                
            }
            
            kimenőCsatorna.write(buffer, 0, olvasottBájtok);//kiirja a titkos szoveget az output streamre
            
        }
        
    }
    
    public static void main(String[] args) {//main fuggveny
        
        try {//megprobalja elinditani a folyamatot
            
            new ExorTitkosító(args[0], System.in, System.out);//a titkosito meghivasa
            
        } catch(java.io.IOException e) {//hiba eseten ez fogja elkapni a hibat
            
            e.printStackTrace();
            
        }
        
    }
    
}