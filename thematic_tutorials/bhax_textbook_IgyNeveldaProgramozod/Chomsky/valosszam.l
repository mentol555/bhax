%{
#include <stdio.h>
int valosszamok=0;
%}
szamjegy    [0-9]
%%
{szamjegy}*(\.{szamjegy}+)?     {++valosszamok;
printf("[[valosszam=%s %f]]", yytext, atof(yytext));}
                                
%%
int main()
{
yylex();
printf("Valos szamok darabszama= %d\n", valosszamok);
return 0;
}
