#include <iostream>
using namespace std;

int *f()//EGESZRE MUTATO MUTATOT VISSZAADO FUGGVENY(7,8as feladathoz)
{
	int a=5;
	int *p=&a;
	cout<<p;
	return p;
}

int main ()
{
	//EGESZ
    int a=5;
	cout<<"1.EGESZ"<<endl<<a;
	cout<<endl<<endl;

	//EGESZRE MUTATO MUTATO
    int b;
	int *p=&b;
	cout<<"2.EGESZRE MUTATO MUTATO"<<endl;
	cout<<p;
	cout<<endl<<endl;

	//EGESZEK REFERENCIAJA
    int x=5;
	int &r=x;
	cout<<"3.EGESZ"<<endl<<a<<endl<<"EGESZ REFERENCIAJA"<<endl;
	cout<<r;
	cout<<endl<<endl;

	//EGESZEK TOMBJE
    int c[5]={1,2,3,4,5};
	cout<<"4.EGESZEK TOMBJE"<<endl;
	for(int i=0;i<5;i++)cout<<c[i]<<" ";
	cout<<endl<<endl;
	cout<<"5.EGESZEK TOMBJENEK REFERENCIAJA:"<<endl;
	int(&tr)[5]=c;
	cout<<tr;
	cout<<endl<<endl;
	
	//EGESZRE MUTATO, MUTATOK TOMBJE
    int *d[5];
	cout<<"6.EGESZRE MUTATO, MUTATOK TOMBJE:"<<endl;
	for(int i=0;i<5;i++){d[i]=&c[i];cout<<d[i]<<" ";}
	cout<<endl<<endl;

	//EGESZRE MUTATO MUTATOT VISSZAADO FUGGVENY, EGESZRE MUTATO MUTATOT VISSZAADO FUGGVENYRE MUTATO MUTATO

	cout<<"7.VISSZAADOTT MUTATO ALTAL MUTATOTT ELEM CIME"<<endl<<f()<<endl<<endl;//kiirodik az "a" memoriacime //egeszre mutato mutatot visszaado fuggveny
    int *(*mut)()=f;//mutato a fuggvenyre
	cout<<"8.FUGGVENY CIME"<<endl<<reinterpret_cast<void*>(mut)<<endl;//kiirodik az f fuggveny memoriacime, itt ezzel a reinterpret_cast-al tudtam csak megoldani.
    return 0;
}
