# BHAX

A BHAX csatorna forráskódjai. 
(Élő adások: https://www.twitch.tv/nbatfai, archívum: https://www.youtube.com/c/nbatfai)
A csatorna célja a szervezett számítógépes játék és a programozás népszerűsítése, tanítása, különös tekintettel a mesterséges intelligenciára! 
Hosszabb távon utánpótlás esport csapatok szervezésének tartalmi támogatása. 

P1TP
__________________________________
| előadás        |     labor     |
| -------------- | ------------- |
| +2 - SMNIST    | +2 CSIGA      |
| +2 - BRAINB    | +3 3.szakkor  | 
| +2 - 36 Pipacs | +3 4.szakkor  |
| +1 - BOGOMIPS  | +2 5x5x5      |
|+7 RFH I(I.hely)| +1 twitch     |
| +1 - E.a exor  | +5 javitott   |
| +1 - exortores | +5 javitottv2 |
| +1 - mutatokul | +10 - RF3kval |
| +2 - 7x7x7     |+10 1.CircleC++|               
| +3 04.01 ea    |+8  2.CircleC++|              
| +3 04.08 ea    |+12 3.CircleC++|              
| +3 04.15 ea    |               |
| +4 debugalloc  |               |
| +6 bogoalloc   | prog2+20xp    |-|
| +6 04.22 ea    | pasz.rapszodia| |
|+30konyvelozetes| opengl-oran   |-|
| +6 04.29 e.a   |               |
|+10 rfh1        |               |
|   tutoraltam   |               |
|    Kiss Davidot|               |
|+20 5x5x5,7x7x7 |               |
|   tutoraltam   |               |
|   Nagy Bianka-t|               |
| +6 05.06 ea    |               |
|+10 rfh4-5virag |               |
|+21 TF-es pelda |               |
|________________|_______________|
       =147               =61