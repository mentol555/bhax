#include <iostream>

using namespace std;

class Lista{

public:
	Lista(){
		cout<<"lista ctor"<<endl;
		fej = new ListaElem();
	}

	~Lista(){
		cout<<"lista dtor"<<endl;
		ListaElem* elem = fej;
		while(elem){
			ListaElem* this_elem = elem;
			elem = elem->kov;
			delete this_elem;
		}
		
	}

	Lista(Lista& old){
		cout<<"masolo konstruktor"<<endl;
		fej = masol(old.fej);
	}
	Lista(Lista&& old){
		cout<<"mozgato konstruktor"<<endl;
		fej = std::move(old.fej);
		old.fej = nullptr;
	}

	Lista& operator=(const Lista& old){
		cout<<"masolo ertekadas"<<endl;
		fej = masol(old.fej);
		return *this;
	}
	Lista& operator=(Lista&& old){
		cout<<"mozgato ertekadas"<<endl;
		fej = old.fej;
		old.fej = nullptr;
		return *this;
	}

	void beszur(int ertek){
		ListaElem* nextElem = fej;
		while(nextElem->kov != nullptr){
			nextElem = nextElem->kov;
		}
		ListaElem* newElem = new ListaElem();
		nextElem->kov = newElem;
		newElem->adat = ertek;
		newElem->kov = nullptr;
	}

	void kiir()
	{
		ListaElem* elem = fej;

			while(elem->kov != nullptr)
			{
				cout<<"elem->adat = "<< elem->adat << "  elem memcime: " <<elem<<endl;
				elem = elem->kov;
			}
		
	}

	class ListaElem{
	public:
		int adat = 0;
		ListaElem* kov = nullptr;
	};

private:
	ListaElem* fej = nullptr;

	ListaElem* masol(ListaElem* elem){
		ListaElem* masolElem;
		if(elem != NULL)
		{
			masolElem = new ListaElem();
			masolElem->adat = elem->adat;
			if(elem->kov != nullptr)
			{
				masolElem->kov = masol(elem->kov);
			}
			else{
				masolElem->kov = nullptr;
			}
		}	
		return masolElem;
	}

};

int main(int argc, char* argv[])
{
		std::cout << "Lista lista; //";
	Lista lista;

	lista.beszur(1);
	lista.beszur(2);

	std::cout << "  Lista :" << std::endl;
	lista.kiir();

	std::cout << "\nLista lista2(lista); //";
	Lista lista2(lista);	
	std::cout << "  Lista2 :" << std::endl;
	lista2.kiir();

	std::cout << "\nLista lista3; //";
	Lista lista3;

	std::cout << "lista3=lista2; //";
	lista3=lista2;

	std::cout << "  Lista3 :" << std::endl;
	lista3.kiir();

	std::cout << "\nLista lista4=std::move(lista3); //";
	Lista lista4(std::move(lista3));

	std::cout << "  Lista4 :" << std::endl;
	lista4.kiir();

	std::cout << "\nLista lista5; //";
	Lista lista5;

	std::cout << "lista5 = std::move(lista4); //";
	lista5 = std::move(lista4);

	std::cout << "  Lista5 :" << std::endl;
	lista5.kiir();

	return 0;
	return 0;
}
