#include <stdio.h>
#include <math.h>

void
kiir (double tomb[], int db)//kiir alprogram
{
int i;
for (i=0; i<db; i++)
printf("PageRank [%d]: %lf\n", i, tomb[i]);
}

double tavolsag(double pr[],double pr_temp[],int db)//tavolsag szamito alprogram, ami kulcsfontossagu, hogy ne menjen orokke a programunk, hanem egy bizonyos feltetel utan alljon le
{
double tav = 0.0;
int i;
for(i=0;i<db;i++)
tav +=(pr[i] - pr_temp[i])*(pr[i] - pr_temp[i]);//ez szamolja ki a bizonyos feltetelt
printf("tav=%f\n",tav);
return sqrt (tav);//visszaadjuk a tavolsag gyoket
}

int main(void)
{
double Lapok[4][4] = {//lapok "viszonymatrixa"
{0.0, 0.0, 1.0 / 3.0, 0.0},
{1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
{0.0, 1.0 / 2.0, 0.0, 0.0},
{0.0, 0.0, 1.0 / 3.0, 0.0}
};

double PageRank[4] = {0.0, 0.0, 0.0, 0.0};//fotomb, amiben lesz a vegeredmeny
double Temp_PageRank[4] = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};//atmeneti tomb, ebben tortennek a szamolasok 

long int i,j;
i=0; j=0;

for (;;)
{
for(i=0;i<4;i++)
PageRank[i] = Temp_PageRank[i];//minden uj ciklusban atadjuk a fotombnek az atmenetiben kiszamolt ertekeket
for (i=0;i<4;i++)
{
double temp=0;
for (j=0;j<4;j++)
temp+=Lapok[i][j]*PageRank[j];//
Temp_PageRank[i]=temp;//atmeneti tomb szamolas
}

if ( tavolsag(PageRank,Temp_PageRank, 4) < 0.000001)//program leallasat vizsgalo feltetel, amelyet fentebb a tavolsag alprogram segit
break;
}
kiir (PageRank,4);//vegertek kiirasa
return 0;

} 