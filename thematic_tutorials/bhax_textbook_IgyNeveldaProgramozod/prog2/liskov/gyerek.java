class szulo 
{
	public void uzenet()
	{
		System.out.println("Szulo uzenete");
	}
};

class gyerek extends szulo 
{
	public void uzenetgyerek()
	{
		System.out.println("Gyerek uzenete");
	}	

	public static void main(String[] args)
	{
		szulo sz1 = new szulo();
		szulo sz2 = new gyerek();
		sz1.uzenet();
		sz2.uzenetgyerek();
	}
};

