package BinFa;


public class JunitTest {

	LZWBinFa binfa = new LZWBinFa(); // letrehozunk manualisan egy fat
	@org.junit.Test

	public void test() {
		String input = "01111001001001000111"; //input bitek amiket hozzaadunk a fahoz
		for (char c : input.toCharArray()) { //bejarjuk az inputot
			binfa.addCsomopont(c); //mindegyik bitet hozzaadjuk
			}
		org.junit.Assert.assertEquals(4, binfa.getMelyseg(), 0.0); //4 ?= melyseg, 0 deltaval
		org.junit.Assert.assertEquals(2.75, binfa.getAtlag(), 0.001); //2.75 ?= atlag, 0.001 deltaval
		org.junit.Assert.assertEquals(0.957427, binfa.getSzoras(), 0.0001); //0.957427 ?= szoras, 0.0001 deltaval
	}
}